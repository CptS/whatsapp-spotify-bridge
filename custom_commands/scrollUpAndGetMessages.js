const EventEmitter = require('events');

/**
 * Custom nightwatch command for scrolling up a whatsapp chat and gathering all messages with spotify links.
 */
class ScrollUpAndGetMessages extends EventEmitter {

    /**
     * Parse whatsapp messages and filter them by containing spotify links and lastScan date.
     * @param lastScan The date (stringified) after which messages should be found.
     * @param cb The callback function.
     */
    parseMessages(lastScan, cb) {
        this.api.execute(function (lastScan) {
            // parse date - only now because by passing to browse it will be stringified
            const lastScanDate = lastScan ? new Date(lastScan) : undefined;

            // get all messages, parse date and skip messages without spotify links
            let items = Array.prototype.slice.call(document.querySelectorAll('div[data-pre-plain-text]'))
                .map(el => {
                    const dateStr = el.getAttribute('data-pre-plain-text').replace(/\[.*,\s*(.*)].*/, '$1');
                    let dateParts, date;
                    if ((dateParts = dateStr.match(/(\d+).(\d+).(\d+)/))) {
                        date = new Date(parseInt(dateParts[3], 10), parseInt(dateParts[2], 10) - 1, parseInt(dateParts[1], 10));
                    } else if ((dateParts = dateStr.match(/(\d+)-(\d+)-(\d+)/))) {
                        date = new Date(parseInt(dateParts[1], 10), parseInt(dateParts[2], 10) - 1, parseInt(dateParts[3], 10));
                    } else if ((dateParts = dateStr.match(/(\d+)\/(\d+)\/(\d+)/))) {
                        date = new Date(parseInt(dateParts[3], 10), parseInt(dateParts[1], 10) - 1, parseInt(dateParts[2], 10));
                    }
                    return {
                        date,
                        links: Array.prototype.slice.call(el.querySelectorAll('a[href^="https://open.spotify.com/track"]'))
                            .map(n => n.getAttribute('href'))
                    };
                })
                .filter(o => o.links.length);

            // if lastScan date is passed filter older messages
            let targetDateReached = false;
            let filteredItems = items;
            if (lastScanDate) {
                filteredItems = filteredItems
                    .filter(o => o.date >= lastScanDate);
                targetDateReached = items.length !== filteredItems.length;
            }

            // order messages and prepare for return to nightwatch
            items = filteredItems
                .sort((a, b) => a.date - b.date)
                .map(o => ({ ...o, date: o.date.toJSON() }));
            return { targetDateReached, items };
        }, [lastScan], (result) => {
            cb(result.value);
        });
    }

    /**
     * Scrolls up the whatsapp messages. This will be done recursively till the top of the history or the first message
     * older than the lastScan date (if passed) was reached.
     * @param chatContainerSelectors The css selector for the chat container.
     * @param loadingIndicatorSelector The css selector for the loading indicator.
     * @param lastScan The last scanDate (stringified).
     * @param recursionCounter The count of the current recursion depth.
     * @param cb The callback function.
     */
    scrollToTop(chatContainerSelectors, loadingIndicatorSelector, lastScan, recursionCounter, cb) {
        this.api.executeAsync(function (chatContainerSelectors, loadingIndicatorSelector, recursionCounter, done) {
            const chatContainer = window.document.querySelector(chatContainerSelectors);
            if (chatContainer.scrollTop === 0 && !document.querySelector(loadingIndicatorSelector)) {
                done({ topReached: true });
            } else {
                chatContainer.scrollTo(0, 0);
                setTimeout(() => done({ topReached: false }), 500);
            }

            const layer = window.document.getElementById('whatsapp-spotify-bridge-layer-text');
            if (layer) {
                if (recursionCounter % 4 === 0) {
                    layer.style.paddingLeft = Math.round((Math.random() * 400) - 200) + 'px';
                    layer.style.marginTop = Math.round((Math.random() * 300) - 150) + 'px';
                }
                layer.style.color = '#' + ((Math.round((Math.random() * 5) + 3) * 10).toString(16))
                    + ((Math.round((Math.random() * 15) + 3) * 10).toString(16))
                    + ((Math.round((Math.random() * 5) + 3) * 10).toString(16));
            }
        }, [chatContainerSelectors, loadingIndicatorSelector, recursionCounter], (scrollResult) => {
            this.parseMessages(lastScan, (messages) => {
                if (scrollResult.value.topReached || messages.targetDateReached) {
                    console.log(messages.targetDateReached ? 'Target date (' + lastScan + ') reached' : 'Top of history reached');
                    cb({
                        messages: messages.items,
                        loops: recursionCounter + 1,
                    });
                } else if (recursionCounter <= 240) {
                    console.log('Neither target date nor top of history reached - keep on scrolling ... (' + recursionCounter + '/240)');
                    this.scrollToTop(chatContainerSelectors, loadingIndicatorSelector, lastScan, recursionCounter + 1, cb);
                } else {
                    console.log('Cannot reach top of history ... Aborting and use all messages found yet.');
                    cb({
                        messages: messages.items,
                        loops: -1,
                    });
                }
            });
        });
    }

    /**
     * The nightwatch command function.
     * @param chatContainerSelectors The css selector for the chat container.
     * @param loadingIndicatorSelector The css selector for the loading indicator.
     * @param lastScan The last scanDate (stringified).
     * @param cb The callback function.
     */
    command(chatContainerSelectors, loadingIndicatorSelector, lastScan, cb) {
        this.scrollToTop(chatContainerSelectors, loadingIndicatorSelector, lastScan, 0, (result) => {
            cb(result);
            this.emit('complete');
        });
    }

}

module.exports = ScrollUpAndGetMessages;
