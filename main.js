const fs = require('fs');
const os = require('os');
const path = require('path');
const express = require('express');
const request = require('request');
const moment = require('moment');

const sessionFile = path.join(os.homedir(), '.whatsapp-spotify-bridge.session');
const lastScanFile = path.join(os.homedir(), '.whatsapp-spotify-bridge.lastScan');

// The group name - this have to be the name of the whatsapp group and the name/prefix of the spotify playlists
const groupName = 'Musi ಥ ͜ʖಥ';

const whatsappGroupSelector = 'span[title*="' + groupName + '"]';
const chatContainerSelectors = '#main>div>.copyable-area>div:first-of-type';
const loadingIndicatorSelector = 'svg>circle';
const whatsappSessionTokenName = 'WAToken1';

const postSpotifyAuthenticationPort = 59071;

const main = function (browser) {
    let sessionData, lastScan, spotifyLinkMessages, spotifyBearerToken, expressServerInstance;

    if (fs.existsSync(sessionFile)) {
        console.log('Session file exists');
        sessionData = JSON.parse(fs.readFileSync(sessionFile, 'utf8'));
        console.log('Session data loaded');
    } else {
        console.log('Session file NOT exists');
    }
    sessionData = sessionData || {};
    sessionData.whatsapp = sessionData.whatsapp || undefined;
    sessionData.spotify = sessionData.spotify || undefined;

    if (fs.existsSync(lastScanFile)) {
        console.log('lastScan file exists');
        lastScan = fs.readFileSync(lastScanFile, 'utf8');
        console.log('lastScan date loaded', lastScan);
    } else {
        console.log('lastScan file NOT exists');
    }

    browser
        // Open WhatsApp Web
        .url('https://web.whatsapp.com/favicon.ico')

        // Restore session, if available:
        .execute(function (sessionData, sessionTokenName) {
            if (sessionData && sessionData[sessionTokenName]) {
                for (let key in sessionData) {
                    if (sessionData.hasOwnProperty(key)) {
                        window.localStorage.setItem(key, sessionData[key]);
                    }
                }
            }
            window.location.href = 'https://web.whatsapp.com/';
            return true;
        }, [sessionData.whatsapp, whatsappSessionTokenName], function () {
            if (sessionData.whatsapp && sessionData.whatsapp[whatsappSessionTokenName]) {
                console.log('Session information available');
            } else {
                console.log('No session information available ...');
            }
        })

        // Wait until QR code is scanned or session was restored:
        .waitForElementVisible('*[data-asset-intro-image], *[data-asset-intro-image-light], *[data-asset-intro-image-dark]', 3 * 60 * 1000)

        // Save session:
        .execute(function () {
            let values = {};
            for (let i = 0, len = window.localStorage.length; i < len; ++i) {
                const key = window.localStorage.key(i);
                values[key] = window.localStorage.getItem(key);
            }

            // show user information:
            const layer = document.createElement("div");
            layer.style = 'pointer-events: none; z-index: 9999; ' +
                'position: absolute; top: 0; right: 0; bottom: 0; left: 0; ' +
                'background-color: rgba(255, 255, 255, .8); color: #000; ' +
                'padding-top: 200px;';
            layer.innerHTML = '<p id="whatsapp-spotify-bridge-layer-text" style="text-align: center;">' +
                'gathering all messages containing spotify links ...<br>' +
                '<small style="font-size: 0.8em;">(especially on first run this could take some time)</small></p>';
            document.body.appendChild(layer);

            return values;
        }, [], function (result) {
            if (!result || result.error || result.value.error) {
                console.error('Error while determining session data:', result);
            } else {
                if (result && result.value && result.value[whatsappSessionTokenName]) {
                    sessionData.whatsapp = result.value;
                    fs.writeFile(
                        sessionFile,
                        JSON.stringify(sessionData),
                        (err) => err && console.error('Error while writing session data:', err)
                    );
                } else {
                    console.error('Invalid session information! Not logged in:', result);
                }
            }
        })

        // Open group:
        .click(whatsappGroupSelector)
        .waitForElementVisible(whatsappGroupSelector, 30 * 1000)
        .click(whatsappGroupSelector)

        // Wait for group content:
        .waitForElementVisible('#main .message-in', 10 * 1000)

        // Get all messages since last scan date
        .scrollUpAndGetMessages(chatContainerSelectors, loadingIndicatorSelector, lastScan, (result) => {
            if (result.loops >= 0) {
                console.log('Required ' + result.loops + ' scroll loops to get to the top of chat history');
            }
            console.log('Found ' + result.messages.length + ' messages with spotify links since ' + (lastScan || 'the beginning'));
            spotifyLinkMessages = result.messages;
        })

        // remember lastScan date:
        .perform(function () {
            if (spotifyLinkMessages && spotifyLinkMessages.length) {
                let newLastScan = spotifyLinkMessages[spotifyLinkMessages.length - 1].date;
                fs.writeFile(
                    lastScanFile,
                    newLastScan,
                    (err) => err && console.error('Error while writing lastScanFile data:', err)
                );
                console.log('lastScan is now ' + newLastScan);
            } else {
                throw "no messages with spotify link found!";
            }
        })

        // End of whatsapp part! Now go to spotify ...
        .perform(function () {
            // Prepare spotify authentication:
            const app = express();
            app.get('/post-spotify-auth', (req, res) => res.send('verify spotify authentication ... <ul id="whatsapp-spotify-bridge-post-spotify-auth"><li>authentication response received</li></ul>'));
            expressServerInstance = app.listen(
                postSpotifyAuthenticationPort,
                () => console.log(`Post spotify authentication page is listening on port ${postSpotifyAuthenticationPort}!`)
            );
        })

        // Open spotify accountstatus page:
        .url('https://accounts.spotify.com/de/status')

        // Restore session, if available:
        .execute(function (sessionData) {
            const layer = document.createElement("div");
            layer.style = 'position: absolute; top: 0; right: 0; bottom: 0; left: 0; background-color: rgba(255, 255, 255, .8); color: #000; text-align: center; padding-top: 200px;';
            layer.innerHTML = 'preparing spotify login ...';
            document.body.appendChild(layer);
            if (sessionData && sessionData.cookies) {
                sessionData.cookies.forEach(cookie => {
                    document.cookie = cookie.name + "=" + cookie.value + '; path=' + cookie.path + '; domain=' + cookie.domain;
                });
            }

            // trigger spotify authentication
            window.location.href = 'https://accounts.spotify.com/authorize?response_type=token&client_id=c7007bcc75374bd99c0f26b03f661d4c&scope='
                + encodeURIComponent('playlist-modify-public user-library-modify')
                + '&redirect_uri=' + encodeURIComponent('http://localhost:59071/post-spotify-auth');
            return true;
        }, [sessionData.spotify], function () {
            if (sessionData.spotify) {
                console.log('Session information available');
            } else {
                console.log('No session information available ...');
            }
        })

        // wait for spotify authentication
        .waitForElementVisible('ul#whatsapp-spotify-bridge-post-spotify-auth', 3 * 60 * 1000)

        // get token
        .execute(function () {
            return {
                bearerToken: window.location.hash.replace(/^#?.*?access_token=(.*?)(?:&.*)?$/i, '$1')
            };
        }, [], function (result) {
            if (expressServerInstance) {
                expressServerInstance.close();
            }

            if (result && result.value && result.value.bearerToken) {
                spotifyBearerToken = result.value.bearerToken;
                console.log('Spotify authentication successful - token: ' + spotifyBearerToken);
            } else {
                console.error('Spotify authentication failed:', result);
            }
        })

        // get cookies and persist session
        .url('https://accounts.spotify.com/de/status')
        .getCookies(function (result) {
            if (result && result.value) {
                sessionData.spotify = { cookies: result.value };
                fs.writeFile(
                    sessionFile,
                    JSON.stringify(sessionData),
                    (err) => err && console.error('Error while writing session data:', err)
                );
            } else {
                console.error('Invalid session information! Not logged in:', result);
            }
        })

        // create playlists
        .execute(function () {
            const layer = document.createElement("div");
            layer.style = 'position: absolute; top: 0; right: 0; bottom: 0; left: 0; ' +
                'background-color: rgba(255, 255, 255, .8); color: #000; padding-top: 200px;';
            layer.innerHTML = '<p id="whatsapp-spotify-bridge-layer-text" style="text-align: center;">' +
                'updating spotify playlists ...<br>' +
                '<small style="font-size: 0.8em;">(especially on first run this could take some time)</small></p>';
            document.body.appendChild(layer);

            const layerP = window.document.getElementById('whatsapp-spotify-bridge-layer-text');
            setInterval(() => {
                layerP.style.paddingLeft = Math.round((Math.random() * 400) - 200) + 'px';
                layerP.style.marginTop = Math.round((Math.random() * 300) - 150) + 'px';
            }, 2000);
            setInterval(() => {
                layerP.style.color = '#' + ((Math.round((Math.random() * 5) + 3) * 10).toString(16))
                    + ((Math.round((Math.random() * 15) + 3) * 10).toString(16))
                    + ((Math.round((Math.random() * 5) + 3) * 10).toString(16));
            }, 500);
        })
        .perform((done) => {
            let spotifyUser,
                userPlaylists,
                userPlaylistTracks = {},
                spotifyApiRequestCounter = 0,
                addedTrackCounter = 0;

            function spotifyApiRequest(options, callback) {
                spotifyApiRequestCounter += 1;
                if (spotifyApiRequestCounter % 60 === 0) {
                    console.log('--- Wait 2000 ms every 60 requests to not overload spotify api ---');
                    setTimeout(() => request(options, callback), 2000);
                } else if (spotifyApiRequestCounter % 20 === 0) {
                    console.log('--- Wait 500 ms every 20 requests to not overload spotify api ---');
                    setTimeout(() => request(options, callback), 500);
                } else {
                    request(options, callback);
                }
            }

            function getPlaylist(name, matchStart, successCb, errorCb) {
                const found = userPlaylists.filter((p) => {
                    if (matchStart) {
                        return p.name.startsWith(name);
                    }
                    return p.name === name;
                });
                if (found.length > 1) {
                    errorCb('Found more than one playlist with name ' + (matchStart ? 'starting with' : 'equals') + ' "' + name + '"');
                } else if (found.length === 1) {
                    successCb(found[0]);
                } else {
                    console.log('create new playlist "' + name + '"');

                    spotifyApiRequest({
                        url: 'https://api.spotify.com/v1/users/' + spotifyUser.id + '/playlists',
                        method: 'POST',
                        headers: {
                            'Authorization': 'Bearer ' + spotifyBearerToken
                        },
                        json: true,
                        body: {
                            name,
                            public: true,
                        }
                    }, (error, response, body) => {
                        if (!error && response.statusCode === 200) {
                            const playlist = JSON.parse(body);
                            console.log('new playlist "' + playlist.name + '" (id: ' + playlist.id + ') created');
                            userPlaylists.push(playlist);
                            successCb(playlist);
                        } else if (!error && response.statusCode === 201) {
                            const newUrl = response.caseless.get('Location');
                            spotifyApiRequest({
                                url: newUrl,
                                headers: {
                                    'Authorization': 'Bearer ' + spotifyBearerToken
                                }
                            }, (error, response, body) => {
                                if (!error && response.statusCode === 200) {
                                    const playlist = JSON.parse(body);
                                    console.log('new playlist "' + playlist.name + '" (id: ' + playlist.id + ') created');
                                    userPlaylists.push(playlist);
                                    successCb(playlist);
                                } else {
                                    errorCb('Error while reading newly created playlist (status: ' + (response ? response.statusCode : 'unknown') + '):', newUrl, error);
                                }
                            });
                        } else {
                            errorCb('Error while creating playlist (status: ' + (response ? response.statusCode : 'unknown') + '):', error);
                        }
                    });
                }
            }

            function loadPlaylistTracks(url, successCb, errorCb, recursiveTracks = []) {
                spotifyApiRequest({
                    url: url,
                    headers: {
                        'Authorization': 'Bearer ' + spotifyBearerToken
                    }
                }, (error, response, body) => {
                    if (!error && response.statusCode === 200) {
                        const tracks = JSON.parse(body);
                        const newTrackIds = tracks.items.map(t => t.track.id);
                        const concatenatedTracks = recursiveTracks.concat(newTrackIds);
                        if (tracks.next) {
                            loadPlaylistTracks(tracks.next, successCb, errorCb, concatenatedTracks);
                        } else {
                            console.log('Loaded ' + concatenatedTracks.length + ' playlist tracks');
                            successCb(concatenatedTracks);
                        }
                    } else {
                        errorCb(error, response);
                    }
                });
            }

            function getPlaylistTracks(playlistId, successCb, errorCb) {
                if (userPlaylistTracks[playlistId]) {
                    successCb(userPlaylistTracks[playlistId]);
                } else {
                    loadPlaylistTracks('https://api.spotify.com/v1/playlists/' + playlistId + '/tracks', (tracks) => {
                        userPlaylistTracks[playlistId] = tracks;
                        successCb(userPlaylistTracks[playlistId]);
                    }, (error, response) => errorCb('Error while reading tracks playlist (id: ' + playlistId + '; status: ' + (response ? response.statusCode : 'unknown') + '):', error));
                }
            }

            function addTrackToPlaylists(track, date, success) {
                moment.locale('de');
                const mom = moment(date);
                const year = mom.year();
                const week = mom.week();
                const targetPlaylists = [
                    [groupName, false],
                    [groupName + ' ' + year, false],
                    [groupName + ' ' + year + ' - ' + mom.format('MMMM'), false],
                    [groupName + ' ' + year + ' - KW' + week, false],
                ];
                if (mom.day() === 4) {
                    targetPlaylists.push([groupName + ' ' + year + ' - KW' + week + ' - Themendonnerstag', true]);
                }

                function loopOverTargetPlaylists(i = 0) {
                    if (targetPlaylists.length > i) {
                        getPlaylist(targetPlaylists[i][0], targetPlaylists[i][1], (playlist) => {
                            getPlaylistTracks(playlist.id, (tracks) => {
                                if (tracks.indexOf(track.id) === -1) {
                                    console.log('Add track "' + track.name + '" (id: ' + track.id + ') to playlist "' + playlist.name + '" (id: ' + playlist.id + ')');
                                    spotifyApiRequest({
                                        url: 'https://api.spotify.com/v1/playlists/' + playlist.id + '/tracks',
                                        method: 'POST',
                                        headers: {
                                            'Authorization': 'Bearer ' + spotifyBearerToken
                                        },
                                        json: true,
                                        body: {
                                            uris: ['spotify:track:' + track.id],
                                        }
                                    }, (error, response) => {
                                        if (error || (response.statusCode !== 200 && response.statusCode !== 201)) {
                                            console.error('Error while adding track to playlist (status: ' + (response ? response.statusCode : 'unknown') + '):', error);
                                        }
                                        addedTrackCounter += 1;
                                        loopOverTargetPlaylists(i + 1);
                                    });
                                } else {
                                    console.log('Track "' + track.name + '" (id: ' + track.id + ') already in playlist "' + playlist.name + '" (id: ' + playlist.id + ') - skipping this track');
                                    loopOverTargetPlaylists(i + 1);
                                }
                            }, (...error) => {
                                console.error("error while loading/creating playlist:", error);
                                loopOverTargetPlaylists(i + 1);
                            });
                        }, (...error) => {
                            console.error("error while loading/creating playlist:", error);
                            loopOverTargetPlaylists(i + 1);
                        });
                    } else {
                        success();
                    }
                }

                loopOverTargetPlaylists();
            }

            function loopMessageLinks(i = 0, j = 0) {
                if (spotifyLinkMessages.length > i) {
                    if (spotifyLinkMessages[i].links && spotifyLinkMessages[i].links.length > j) {
                        const trackId = spotifyLinkMessages[i].links[j].replace(/^https:\/\/open.spotify.com\/track\/(.*?)(?:\?.*)$/, '$1');

                        if (trackId) {
                            spotifyApiRequest({
                                url: 'https://api.spotify.com/v1/tracks/' + trackId,
                                headers: {
                                    'Authorization': 'Bearer ' + spotifyBearerToken
                                }
                            }, (error, response, body) => {
                                if (!error && response.statusCode === 200) {
                                    const track = JSON.parse(body);
                                    console.log('Add track "' + track.name + '" (id: ' + track.id + ') to library if not already added');
                                    spotifyApiRequest({
                                        url: 'https://api.spotify.com/v1/me/tracks?ids=' + trackId,
                                        method: 'PUT',
                                        headers: {
                                            'Authorization': 'Bearer ' + spotifyBearerToken
                                        }
                                    }, (error, response) => {
                                        if (!error && response.statusCode === 200) {
                                            addTrackToPlaylists(track, new Date(spotifyLinkMessages[i].date), () => loopMessageLinks(i, j + 1));
                                        } else {
                                            console.error('Error while adding track (id: ' + trackId + ') to library (status: ' + (response ? response.statusCode : 'unknown') + ')', error);
                                            loopMessageLinks(i, j + 1);
                                        }
                                    });
                                } else {
                                    console.error('Error while getting track by id (status: ' + (response ? response.statusCode : 'unknown') + '):' + trackId, error);
                                    loopMessageLinks(i, j + 1);
                                }
                            });
                        } else {
                            console.error('Can\'t get track id from url:', spotifyLinkMessages[i].links[j]);
                            loopMessageLinks(i, j + 1);
                        }
                    } else {
                        loopMessageLinks(i + 1, 0);
                    }
                } else {
                    console.log('====================================================');
                    if (addedTrackCounter) {
                        console.log('Finished! 🎉🎉🎉');
                        console.log(addedTrackCounter + ' track playlist relations was created.');
                    } else {
                        console.log('Finished but no new tracks are added.');
                    }
                    console.log('====================================================');
                    done();
                }
            }


            function loadPlaylists(nextUrl = 'https://api.spotify.com/v1/me/playlists?limit=50&offset=0', items = []) {
                spotifyApiRequest({
                    url: nextUrl,
                    headers: {
                        'Authorization': 'Bearer ' + spotifyBearerToken
                    }
                }, (error, response, body) => {
                    if (!error && response.statusCode === 200) {
                        const playlistResponse = JSON.parse(body);
                        if (playlistResponse.next) {
                            loadPlaylists(playlistResponse.next, items.concat(playlistResponse.items));
                        } else {
                            userPlaylists = items.concat(playlistResponse.items);
                            loopMessageLinks();
                        }
                    } else {
                        console.error('Error while getting playlists of current user (status: ' + (response ? response.statusCode : 'unknown') + '):', error);
                    }
                });
            }

            spotifyApiRequest({
                url: 'https://api.spotify.com/v1/me',
                headers: {
                    'Authorization': 'Bearer ' + spotifyBearerToken
                }
            }, (error, response, body) => {
                if (!error && response.statusCode === 200) {
                    spotifyUser = JSON.parse(body);
                    console.log('Logged in as "' + spotifyUser.display_name + '" (id: ' + spotifyUser.id + ')');
                    loadPlaylists();
                } else {
                    console.error('Error while loading current user info (status: ' + (response ? response.statusCode : 'unknown') + '):', error);
                }
            });
        })

        .end(() => {
            if (process.platform === 'win32') {
                console.log('Manually closing geckodriver because we\'re on windows ...');
                const { exec } = require("child_process");
                exec('taskkill /IM "geckodriver.exe" /F');
            }
        });
};

// noinspection JSUnusedGlobalSymbols
module.exports = { main };
