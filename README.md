![WhatsApp-Spotify-Bridge](https://assets.gitlab-static.net/uploads/-/system/project/avatar/12490003/logo-1564596436583.png?width=64)

## What's that?

It's tool to read all Messages from a whatsapp group and searches for links to songs on spotify.
Depending on the found links Playlists will be created in a dedicated user account.

### Features

- Persistent sessions
- Only load Messages after last run
- Create multiple playlists:
    - All songs
    - Per year
    - Per month
    - Per week in year
    - And a special on for _Themendonnerstag_
      (The name of the _Themendonnerstag_ playlist could be suffixed manually, e. g. to add the topic)

## Preconditions

- Firefox (or Chrome, see [Run with Chrome](#run-with-chrome))
- Node.js / npm
- User accounts for Whatsapp and Spotify
- Graphical desktop environment required for whatsapp login (QR code)

## Run bridge

Prepare:

```bash
git clone git@gitlab.com:CptS/whatsapp-spotify-bridge.git
npm install
```

And run:

```bash
npm run bridge
```

### Run with Chrome

To use Chrome instead of Firefox you have to

1. rename `nightwatch.json` to `nightwatch_firefox.json`
2. rename `nightwatch_chrome.json` to `nightwatch.json`
3. remove `geckodriver` dependency in `package.json`
4. add `"chromedriver": "^2.46.0"` dependency in `package.json`
5. run `npm install`

Now you can run the bridge as usually:

```bash
npm run bridge
```
